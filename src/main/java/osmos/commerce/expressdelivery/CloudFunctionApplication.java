package osmos.commerce.expressdelivery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = {"osmos.commerce"})
@EntityScan(basePackages = {"osmos.commerce"})
@SpringBootApplication(scanBasePackages = {"osmos.commerce.expressdelivery.repository",
        "osmos.commerce.expressdelivery.usecase",
        "osmos.commerce.expressdelivery"})
public class CloudFunctionApplication {
    public static void main(String[] args) {
        SpringApplication.run(CloudFunctionApplication.class, args);
    }

}
