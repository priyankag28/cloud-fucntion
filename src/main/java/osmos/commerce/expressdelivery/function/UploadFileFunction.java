package osmos.commerce.expressdelivery.function;

import lombok.extern.slf4j.Slf4j;
import osmos.commerce.expressdelivery.model.UploadDownloadRequestModel;
import osmos.commerce.expressdelivery.model.UploadDownloadResponseModel;
import osmos.commerce.expressdelivery.usecase.GddFileUploadDownloadUseCase;
import java.util.function.Function;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class UploadFileFunction implements Function<UploadDownloadRequestModel, UploadDownloadResponseModel> {

    @Autowired
    GddFileUploadDownloadUseCase fileUpload;

    @Override
    public UploadDownloadResponseModel apply(UploadDownloadRequestModel request) {

        String publicDownloadUrl=fileUpload.getPublicDownloadUrl(request);

        if(!StringUtils.isBlank(publicDownloadUrl))
        {
            return UploadDownloadResponseModel.builder()
                    .publicDownloadUrl(publicDownloadUrl)
                    .build();
        }
        log.info("Signed Download Url {} : DeliveryId {}", publicDownloadUrl , request.getDeliveryId());
        return UploadDownloadResponseModel.builder()
                .publicDownloadUrl("Download link not available")
                .build();
    }
}
