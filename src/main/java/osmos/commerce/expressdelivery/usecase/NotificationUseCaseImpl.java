package osmos.commerce.expressdelivery.usecase;

import osmos.commerce.expressdelivery.model.CreateNotificationRequestModel;
import osmos.commerce.expressdelivery.model.NotificationResponseModel;
import osmos.commerce.expressdelivery.model.NotificationStatusResponse;
import osmos.commerce.expressdelivery.model.UploadDownloadRequestModel;
import osmos.commerce.expressdelivery.repository.NotificationRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriBuilder;
import reactor.core.publisher.Mono;

@Repository
@Slf4j
public class NotificationUseCaseImpl implements NotificationRepository {

    private String NOTIFICATION_DOMAIN_HOST = "www.osmos.services";
    private String SCHEME = "https";
    private String TENANT_HEADER_KEY = "x-tenant-id";
    private String AUTH_HEADER_KEY = "Authorization";

    private WebClient webClient = WebClient.builder().build();

    @Override
    public Mono<NotificationStatusResponse> getNotificationUpdates(UploadDownloadRequestModel request, String notificationId) {
        return webClient
                .get()
                .uri(uriBuilder -> createBaseUri(uriBuilder).path("/notification/v1/notifications/{notificationId}").build(notificationId))
                .header(TENANT_HEADER_KEY, request.getTenantId())
                .header(AUTH_HEADER_KEY, request.getNotificationAuthToken())
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals, response -> Mono.error(new Exception("Get Notification status 404!!")))
                .bodyToMono(NotificationStatusResponse.class);
    }

    @Override
    public Mono<NotificationResponseModel> sendNotification(UploadDownloadRequestModel request, CreateNotificationRequestModel notificationRequest) {

        log.info("Sending Email Notification UploadDownloadRequestModel with data {}" ,notificationRequest);

        return webClient
                .post()
                .uri(uriBuilder -> createBaseUri(uriBuilder).path("/notification/v1/notifications/email").build())
                .header(TENANT_HEADER_KEY, request.getTenantId())
                .header(AUTH_HEADER_KEY, request.getNotificationAuthToken())
                .bodyValue(notificationRequest)
                .exchange()
                .flatMap(response -> {
                    if (response.statusCode().isError()) {
                        return Mono.error(new Exception("Exception on Connecting Email send notification !!"));
                    }
                    return response.bodyToMono(NotificationResponseModel.class);
                })
                .switchIfEmpty(
                        Mono.error(new Exception("No UploadDownloadResponseModel from Post Notification!!"))
                );
    }

    private UriBuilder createBaseUri(UriBuilder uriBuilder) {

        return uriBuilder
                .host(NOTIFICATION_DOMAIN_HOST)
                .scheme(SCHEME);
    }
}
