package osmos.commerce.expressdelivery.usecase;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import osmos.commerce.expressdelivery.entity.AsigneeEntity;
import osmos.commerce.expressdelivery.model.EmailContentModel;
import osmos.commerce.expressdelivery.model.CreateNotificationRequestModel;
import osmos.commerce.expressdelivery.model.CreateNotificationDataModel;
import osmos.commerce.expressdelivery.model.NotificationResponseModel;
import osmos.commerce.expressdelivery.model.EmailModel;
import osmos.commerce.expressdelivery.model.NotificationStatusResponse;
import osmos.commerce.expressdelivery.model.UploadDownloadRequestModel;
import osmos.commerce.expressdelivery.model.EmailAttributeModel;
import osmos.commerce.expressdelivery.model.NotificationRequestModel;
import osmos.commerce.expressdelivery.model.EmailTemplatesModel;
import osmos.commerce.expressdelivery.model.RecipientUserModel;
import osmos.commerce.expressdelivery.repository.NotificationRepository;

@Slf4j
@RequiredArgsConstructor
@Component
public class NotificationRequestUsecase {

    private static String FA_EMAIL_TEMPLATE = "FACL_RT_CR_SubOrden_SDS";
    private static String EMAIL_ACTION = "transactional_email";

    private final NotificationRepository webClientRepository;

    NotificationResponseModel sendEmailNotificationWithDownloadUrl(String downloadUrl, AsigneeEntity asigneeEntity, UploadDownloadRequestModel request) {

        EmailModel email = EmailModel.builder().build();
        asigneeEntity.getCustomInfo().forEach(customInfo -> {
            if (customInfo.getName().contains("email")) {
                String emailId = customInfo.getValues().stream().findFirst().get();
                email.setRecipient(emailId);
            }
        });
        log.info(" Sending email to {} ", email.getRecipient());

        NotificationRequestModel notificationRequest = NotificationRequestModel.builder()
                .attributes(EmailAttributeModel.builder()
                        .to(email)
                        .template(EmailTemplatesModel.builder().id(FA_EMAIL_TEMPLATE).build())
                        .build())
                .action(EMAIL_ACTION)
                .content(
                        EmailContentModel.builder()
                                .user(
                                    RecipientUserModel.builder()
                                    .email(email.getRecipient())
                                    .name(asigneeEntity.getName())
                                    .downlaodUrl(downloadUrl)
                                    .build())
                                .build()
                )
                .build();

        return webClientRepository.
                sendNotification(request, CreateNotificationRequestModel.builder()
                        .data(
                                CreateNotificationDataModel.builder()
                                        .notification(notificationRequest)
                                        .build()
                        ).build()).block();
    }

    NotificationStatusResponse getNotificationUpdateById(UploadDownloadRequestModel request, String notificationId) {
        return webClientRepository.getNotificationUpdates(request, notificationId).block();
    }
}
