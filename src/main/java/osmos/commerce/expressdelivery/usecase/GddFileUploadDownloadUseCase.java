package osmos.commerce.expressdelivery.usecase;

import java.util.Collections;
import osmos.commerce.expressdelivery.entity.AsigneeEntity;
import osmos.commerce.expressdelivery.model.RecipientAttributesModel;
import osmos.commerce.expressdelivery.model.NotificationResponseModel;
import osmos.commerce.expressdelivery.model.NotificationStatusModel;
import osmos.commerce.expressdelivery.model.NotificationStatusResponse;
import osmos.commerce.expressdelivery.model.StorageDownloadUrlModel;
import osmos.commerce.expressdelivery.model.UploadDownloadRequestModel;
import osmos.commerce.expressdelivery.model.StatusesResponseModel;
import osmos.commerce.expressdelivery.model.TypeAssignee;
import osmos.commerce.expressdelivery.repository.AsigneeRepository;
import com.google.cloud.storage.Acl;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;


@Service
@Slf4j
@RequiredArgsConstructor
public class GddFileUploadDownloadUseCase {

    private static String BUCKET_NAME = "ace-server-290510.appspot.com";
    private static Integer  EXPIRE_HOURS_NO = 24;
    private static String STORAGE_END_POINT= "https://storage.cloud.google.com/";
    private static String URL_SEPERATOR = "/";
    private static String FILE_SEPERATOR = "_";

    @Autowired
    private AsigneeRepository asigneeRepository;

    @Autowired
    private Storage storage;


    @Autowired
    NotificationRequestUsecase notificationWebClientRequest;

    public String getPublicDownloadUrl(UploadDownloadRequestModel request) {

        Optional<AsigneeEntity> assigneeEntity = asigneeRepository.
                getPendingNotificationDelivery("PENDING", UUID.fromString(request.getDeliveryId()) , "driver");
        if (assigneeEntity.isPresent()) {
            return createAndSaveNotificationWithDownloadUrl(assigneeEntity.get().getDeliveryId(), assigneeEntity.get().getCustomInfo(), assigneeEntity.get(), request);
        }
        log.info("No Pending status found for delivery {}", request.getDeliveryId());
        return null;
    }

    private String createAndSaveNotificationWithDownloadUrl(UUID deliveryId, List<RecipientAttributesModel> customInfo, AsigneeEntity asigneeEntity, UploadDownloadRequestModel request) {
        String signedUrlFromStorage=null;
        if (!customInfo.isEmpty()) {
            RecipientAttributesModel attributes = customInfo.stream()
                    .filter(attribute -> attribute.getName().contains("privateDownloadUrl")).findFirst().orElse(null);

            if (attributes != null) {
                String privateDownloadUrl = attributes.getValues().stream().findFirst().orElse(null);
                log.info("Private Download Url {} from Dispatch for delivery {}" , privateDownloadUrl , deliveryId);

                signedUrlFromStorage = validatePrivateUrlAndUploadGddData(privateDownloadUrl);

                if (!StringUtils.isBlank(signedUrlFromStorage)) {
                    List<StatusesResponseModel> statuses = sendAndGetEmailNotification(signedUrlFromStorage, asigneeEntity, request);
                    saveNewDownloadUrl(deliveryId, signedUrlFromStorage, statuses);
                }
            }else {
                log.info("Private Download Url is missing!!");
            }
        }
        log.info("New Signed Download Url {}" ,signedUrlFromStorage);
        return signedUrlFromStorage;
    }

    private String validatePrivateUrlAndUploadGddData(String privateDownloadUrl) {
        Resource downloadFileData =null;
        byte[] dataInByte = null;
        try {
            URL url = new URL(privateDownloadUrl);
            downloadFileData = new UrlResource(url);

            String[] privateGddFileName = url.getFile().split("/");
            String gddSourceFileName = privateGddFileName[2].trim();

            log.info("GDD Source file name {} ",gddSourceFileName);
            dataInByte = downloadFileData.getInputStream().readAllBytes();
            return uploadAndCreateNewSignedUrl(dataInByte, gddSourceFileName);

        } catch (IOException e) {
            log.error("Error in parsing privateDownloadUrl {} ", privateDownloadUrl);
        }

        return null;
    }

    private String uploadAndCreateNewSignedUrl(byte[] downloadedGddData, String gddFileName) {
        try {
            BlobId blobId = BlobId.of(BUCKET_NAME, UUID.randomUUID().toString().concat(FILE_SEPERATOR).concat(gddFileName));
            BlobInfo blobInfo = BlobInfo.newBuilder(blobId).build();

            //upload gdd data in cloud-bucket
            storage.create(blobInfo, downloadedGddData);
            //provide public read access to gdd doc
            storage.createAcl(blobId, Acl.of(Acl.User.ofAllUsers(), Acl.Role.READER));

            StorageDownloadUrlModel downloadUrlModel = StorageDownloadUrlModel.builder()
                    .signUrl(storage.signUrl(blobInfo, EXPIRE_HOURS_NO, TimeUnit.HOURS))
                    .authUrl(STORAGE_END_POINT.concat(BUCKET_NAME).concat(URL_SEPERATOR).concat(gddFileName))
                    .publicUrl(STORAGE_END_POINT.concat(BUCKET_NAME).concat(URL_SEPERATOR).concat(gddFileName)).build();

            log.info("Storage download urls created successfully !! {}", downloadUrlModel.toString());
            if (downloadUrlModel.getSignUrl() != null) {
                return downloadUrlModel.getSignUrl().toString();
            }
        }
        finally {
            downloadedGddData = null;
            log.info("Resource cleared !! {} ",downloadedGddData);
        }
        return null;
    }

    private void saveNewDownloadUrl(UUID deliveryId, String url, List<StatusesResponseModel> statuses) {
        List<Optional<AsigneeEntity>> drivers = asigneeRepository.findByDeliveryIdAndType(deliveryId, TypeAssignee.DRIVER.getValue());
        drivers.stream()
                .filter(Optional::isPresent)
                .map(Optional::get)
                .findFirst()
                .ifPresent(driver -> {
                    List<String> values = new ArrayList<>();
                    values.add(url);

                    Optional<RecipientAttributesModel> attribute = driver.getCustomInfo().stream().filter(attributeObject -> attributeObject.getName().contains("publicDownloadUrl")).findFirst();
                    if (attribute.isEmpty()) {
                        driver.getCustomInfo().add(RecipientAttributesModel.builder().name("publicDownloadUrl").type("String").values(values).build());
                    }
                    driver.getCustomInfo().stream()
                            .filter(object -> object.getName()
                                    .contains("publicDownloadUrl")).forEach(downloadUrlUpdate -> downloadUrlUpdate.setValues(values));

                    driver.setNotificationStatus(NotificationStatusModel.builder()
                            .status("SENT")
                            .statuses(statuses)
                            .build());
                });
        List<AsigneeEntity> driver = drivers.stream()
                .filter(Optional::isPresent).
                        map(Optional::get).collect(Collectors.toList());
        asigneeRepository.saveAll(driver);
    }

    private List<StatusesResponseModel> sendAndGetEmailNotification(String downloadUrl, AsigneeEntity asigneeEntity, UploadDownloadRequestModel request) {
        NotificationResponseModel notificationResponse = notificationWebClientRequest.sendEmailNotificationWithDownloadUrl(downloadUrl, asigneeEntity, request);
        if(Optional.ofNullable(notificationResponse.getData()).isPresent() && Optional.ofNullable(notificationResponse.getData().getNotification()).isPresent()){
                String notificationId = notificationResponse.getData().getNotification().getId();

                log.info("Notification id generated {} for delivery {} " ,notificationId, request.getDeliveryId());

                //check status : this can't be updated one ,SENT status may reflect later
                NotificationStatusResponse getNotificationStatuses = notificationWebClientRequest.getNotificationUpdateById(request,
                        notificationId);
                StatusesResponseModel statusesResponseModel = StatusesResponseModel.builder().build();
                getNotificationStatuses.getData().getNotification().getStatuses().forEach(status -> statusesResponseModel.setStatus(status.getStatus()));

                return getNotificationStatuses.getData().getNotification().getStatuses();
        }
        return Collections.emptyList();
    }

}