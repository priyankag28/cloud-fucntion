package osmos.commerce.expressdelivery.repository;

import osmos.commerce.expressdelivery.model.CreateNotificationRequestModel;
import osmos.commerce.expressdelivery.model.NotificationResponseModel;
import osmos.commerce.expressdelivery.model.NotificationStatusResponse;
import osmos.commerce.expressdelivery.model.UploadDownloadRequestModel;
import reactor.core.publisher.Mono;


public interface NotificationRepository {

    Mono<NotificationStatusResponse> getNotificationUpdates(UploadDownloadRequestModel request, String notificationId);

    Mono<NotificationResponseModel> sendNotification(UploadDownloadRequestModel request, CreateNotificationRequestModel notificationRequest);

}
