package osmos.commerce.expressdelivery.repository;

import osmos.commerce.expressdelivery.entity.AsigneeEntity;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AsigneeRepository extends JpaRepository<AsigneeEntity, UUID> {

    List<Optional<AsigneeEntity>> findByDeliveryIdAndType(UUID deliveryId, String type);

    @Query(value = "SELECT * FROM asignee a WHERE a.notification_status ->> 'status' = ?1 AND a.delivery_id = ?2 AND a.type = ?3", nativeQuery = true)
    Optional<AsigneeEntity> getPendingNotificationDelivery(String status, UUID deliveryId , String type);

}
