package osmos.commerce.expressdelivery.entity;
import osmos.commerce.expressdelivery.model.RecipientAttributesModel;
import osmos.commerce.expressdelivery.model.NotificationStatusModel;
import osmos.commerce.expressdelivery.model.BsonType;
import java.util.List;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

@Data
@Entity
@Table(name = "asignee")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TypeDef(name = BsonType.BSON, typeClass = BsonType.class)
public class AsigneeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    @Column(name = "delivery_id")
    private UUID deliveryId;
    @Column(name = "name")
    private String name;
    @Column(name = "profile_picture")
    private String profilePicture;
    @Column(name = "type")
    private String type;
    @Column(name = "custom_info")
    @Type(type = BsonType.BSON)
    private List<RecipientAttributesModel> customInfo;
    @Column(name = "notification_status")
    @Type(type = BsonType.BSON)
    private NotificationStatusModel notificationStatus;
}
