package osmos.commerce.expressdelivery.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StatusInfoModel {

    private String to;
    private String status;
    private String messageKey;
    private String subscriberKey;
    private String renderedSubject;
}
