package osmos.commerce.expressdelivery.model;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NotificationModel {
    String id;
    String externalKey;
    String action;
    EmailAttributesModel attributes;
    List<StatusesResponseModel> statuses;
}
