package osmos.commerce.expressdelivery.model;

public enum NotificationStatus {

    INIT("INIT"),
    PROCESSING("PROCESSING"),
    FAILED("FAILED"),
    QUEUED("QUEUED"),
    SENT("SENT"),
    TYPE("NOT_SENT"),
    BOUNCED("BOUNCED");

    private String value;

    NotificationStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
