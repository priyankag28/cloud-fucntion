package osmos.commerce.expressdelivery.model;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RecipientAttributesModel {

    private String id;
    private String name;
    private String type;
    private String description;
    private List<String> values;
    private Integer order;
    private String group;
    private Boolean isMandatory;
    private Object metadata;
}

