package osmos.commerce.expressdelivery.model;

public enum TypeAssignee {
    DRIVER("driver");

    private String value;

    TypeAssignee(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
