package osmos.commerce.expressdelivery.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RecipientUserModel {

    private String name;
    private String email;
    private String lastname;
    private String downlaodUrl;
}
