package osmos.commerce.expressdelivery.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NotificationCompositeModel {

    private String jobID;
    private String listId;
    private String batchId;
    private String emailId;
    private String subscriberId;
}
