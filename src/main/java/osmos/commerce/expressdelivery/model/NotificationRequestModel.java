package osmos.commerce.expressdelivery.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NotificationRequestModel {

    private String action;
    private EmailAttributeModel attributes;
    private EmailContentModel content;

}
