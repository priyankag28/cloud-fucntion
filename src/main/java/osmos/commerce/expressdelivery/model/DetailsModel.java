package osmos.commerce.expressdelivery.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DetailsModel {
    private String eid;
    private String mid;
    private StatusInfoModel info;
    private NotificationCompositeModel composite;
    private String compositeId;
    private String definitionId;
    private String timestampUTC;
    private String definitionKey;
    private String eventCategoryType;
    private String sendClassificationType;
}
