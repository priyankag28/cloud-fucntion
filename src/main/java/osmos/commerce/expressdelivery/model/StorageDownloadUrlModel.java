package osmos.commerce.expressdelivery.model;

import java.net.URL;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StorageDownloadUrlModel {

    private URL signUrl;
    private String authUrl;
    private String publicUrl;
}