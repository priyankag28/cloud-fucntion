package osmos.commerce.expressdelivery.model;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NotificationStatusModel {
    private String status;
    private List<StatusesResponseModel> statuses;
}

