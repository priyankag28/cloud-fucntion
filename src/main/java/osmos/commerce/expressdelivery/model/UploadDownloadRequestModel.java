package osmos.commerce.expressdelivery.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UploadDownloadRequestModel {

    private String privateDownloadUrl;
    private String tenantId;
    private String notificationAuthToken;
    private String deliveryId;
}
