package osmos.commerce.expressdelivery;

import java.io.IOException;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.web.client.TestRestTemplate;

import static org.assertj.core.api.Assertions.assertThat;

public class GcpFunctionIntegrationTest {

    private TestRestTemplate rest = new TestRestTemplate();

    @Test
    public void testSample() throws IOException, InterruptedException {
        try (LocalServerTestSupport.ServerProcess process = LocalServerTestSupport.startServer(CloudFunctionApplication.class)) {
            String result = rest.postForObject("http://localhost:8080/", "express", String.class);
            assertThat(result).isEqualTo("\"express\"");
        }
    }
}
